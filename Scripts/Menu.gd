extends Control

func _ready():
	#So arrow keys and enter can select gamemode
	$MenuButtons/PvpButton.grab_focus() 

func _on_PvpButton_pressed():
	GlobalVars.ai_or_player = 'player' #Accessed by BatRight.gd.
	get_tree().change_scene('res://Pong.tscn')

func _on_PvcButton_pressed():
	GlobalVars.ai_or_player = 'ai' #As above.
	get_tree().change_scene('res://Pong.tscn')

func _on_Quit_pressed():
	get_tree().quit(0)
