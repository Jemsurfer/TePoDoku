extends Node
var points_left = 0
var points_right = 0

func _on_AreaL_body_entered(_body):
	reset_ball('left')
	
func _on_AreaR_body_entered(_body):
	reset_ball('right')
	
func _process(_delta):
	$Points/PointsL.text = str(points_left)
	$Points/PointsR.text = str(points_right)

func reset_ball(side):
	$Sfx/ResetBallBeep.play()
	match side:
		'left':
			points_right += 1
		'right':
			points_left += 1
	if points_left >= 10 || points_right >= 10:
		if points_left > points_right:
			GlobalVars.whowon = "Left" #Accessed by GameOver.gd 
			get_tree().change_scene('res://GameOver.tscn')
		if points_right > points_left:
			GlobalVars.whowon = "Right" #Ditto
			get_tree().change_scene("res://GameOver.tscn")
	#Same as in Ball.gd, randomize ball velocity, and reset position.
	get_tree().call_group('BallGroup', '_ready')
