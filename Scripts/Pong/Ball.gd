extends KinematicBody2D

var speed = GlobalVars.speed
var velocity = Vector2.ZERO
onready var timer = get_node('/root/Pong/Timer')

func _ready():
	#Random starting velocities for the ball (25% chance for 1 direction)
	#And ensure position of ball is centre.
	speed = 0
	position = Vector2(960, 540)
	randomize()
	velocity.x = [-1,1][randi() % 2] 
	velocity.y = [-0.5,0.5][randi() % 2]
	timer.start()
	
func _physics_process(delta):
	var collision_object = move_and_collide(velocity * speed * delta)
	if collision_object:
		velocity = velocity.bounce(collision_object.normal) #Bounce on collision
		var Speed_increase = GlobalVars.speed * 1.09 #Increase speed by 9%
		if Speed_increase > 50:#So it doesn't speed up too much
			GlobalVars.speed += 25
		else:
			GlobalVars.speed += Speed_increase

func _on_Timer_timeout():
	speed = GlobalVars.speed
	_physics_process(get_physics_process_delta_time())
