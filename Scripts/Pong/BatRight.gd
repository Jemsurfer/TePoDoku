extends KinematicBody2D
onready var ball = get_node('/root/Pong/Ball')

func _physics_process(delta):
	match GlobalVars.ai_or_player:
		'ai':
			move_and_slide(Vector2(0, ball_vs_ai_position()) * GlobalVars.speed)
		'player':
			var yvel = 0
			if Input.is_key_pressed(KEY_DOWN):
				yvel = 1
			if Input.is_key_pressed(KEY_UP):
				yvel = -1
			move_and_slide(Vector2(0, yvel * GlobalVars.speed))	

func ball_vs_ai_position():
	#Very basic AI; if the ball's y position is more than 25 pixels away 
	#from the bat's y position, then move.
	#Move up if ball is above bat, and down if ball is below bat.
	if abs(ball.position.y - position.y) > 25:
		if ball.position.y > position.y:
			return 1
		else:
			return -1
	else:
		return 0
