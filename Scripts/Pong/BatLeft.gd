extends KinematicBody2D

func _physics_process(_delta):
	var yvel = 0
	if Input.is_key_pressed(KEY_W):
		yvel = -1
	if Input.is_key_pressed(KEY_S):
		yvel = 1
	#Use the same speed as ball
	move_and_slide(Vector2(0, yvel * GlobalVars.speed))
