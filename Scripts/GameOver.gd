extends Control

func _ready():
	$Buttons/PlayAgain.grab_focus()
	$WhoWon.text = GlobalVars.whowon + ' wins!'

func _on_PlayAgain_pressed():
	get_tree().change_scene("res://Main_Scene.tscn")

func _on_Quit_pressed():
	get_tree().quit(0)
	
