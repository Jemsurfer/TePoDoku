# TePoDoku

Tetris + Pong + Sudoku = TePoDoku!

## Premise
The game can be played solo, or competitively.

Solo:
- It begins with a simple game of tetris.
- Upon losing, you enter a game of pong (against the computer) or sudoku (decided randomly). The result of this game determines whether you get another go at tetris.
- The game ends when the user loses.

Multiplayer:
- Players decide at the start what the point goal should be.
- Tetris split-screen. If one player loses, they play sudoku until they win (and begin tetris again), or the other player loses at tetris. 
- When both players have lost at tetris, a game of pong begins. The winner of this pong match gains a point. The process repeats until point goal is achieved.
